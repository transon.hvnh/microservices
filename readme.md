## Setup Env

1. install azure cli and login your account
```bash
az login
```
2. Get resource group name

```bash
az group list || az group list -otable
```

You can see resource group in "name", ex: 1-ae674bbb-playground-sandbox 

3. Create AKS

```bash
az aks create -g ${your-resource-group-here} -n ${name-cluster-here} --generate-ssh-keys --node-count 2
```
4. Get credential
```bash
az aks get-credentials -g ${your-resource-group-here} -n ${name-cluster-here} --overwrite-existing
```

5. Build and run dockerfile
```bash
docker build -t jenkins-cicd . && docker run -v /your/path/:/var/jenkins_home:/var/jenkins_home -p 8080:8080 jenkins-cicd
```

6. Setup Jenkins with suggestion plugins and install "Kubernetes CLI" plugins 

- Manage Jenkins > Plugin Manager -> Available  -> Install Kubernetes CLI

7. Add credential Kubernetes in Jenkins

- Manage Jenkins > Manage Credentials -> select Jenkins with domain Global  -> select "Global credentials (unrestricted)" -> Add Credentials -> Kind: secret file -> choose file in local: $HOME/.kube/config -> ID: "k8s" 

8. Get ServerUrl AKS - value have key name "server", ex: server: https://myclustero-1-1f2aa9ef15a2e2.hcp.westus.azmk8s.io:443

```bash
cat $HOME/.kube/config
```

9. Create pipeline 
- jenkins home -> new items -> select pipeline and name -> input script
```bash
pipeline {
    agent any
    stages {
        stage('checkout') {
            steps {
                checkout([$class: 'GitSCM', branches: [[name: '*/main']], extensions: [], userRemoteConfigs: [[url: 'https://gitlab.com/transon.hvnh/microservices.git']]])
            }
        }
        
        stage('Apply Kubernetes files') {
            steps {
            withKubeConfig([credentialsId: 'k8s', serverUrl: '${your-server-url-aks}]) {
              sh 'kubectl apply -f deployment.yaml'
            }
          }
        }
    }
}
```


10. You can check the address in azure portal (Home -> AKS -> Your cluster -> Services and ingress -> service name: frontend-external -> Get public 


